# Visión por computador
# Detectar formas con Python

# importar librerías
import numpy as np
import cv2

# lectura de archivos de imagen
# lectura de la imagen en color
#img = cv2.imread('shapes.png')
img = cv2.imread('formas.png')
# lectura de la imagen en escala de grises
#gray = cv2.imread('shapes.png',0)
gray = cv2.imread('formas.png',0)
# guardar las imágenes en carpetas diferentes(./color y ./gray)

# analizar las funciones threshold, findContours, approxPolyDP y arcLength (parámetros, entradas, salidas, tipos de datos, umbrales, valores por defecto )
ret,thresh = cv2.threshold(gray,127,255,1)

contours,h = cv2.findContours(thresh,1,2)

for cnt in contours:
    approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
    print (len(approx))
    if len(approx)==5:
        print ("pentagon")
        cv2.drawContours(img,[cnt],0,255,-1)
    elif len(approx)==3:
        print ("triangle")
        cv2.drawContours(img,[cnt],0,(0,255,0),-1)
    elif len(approx)==4:
        print ("square")
        cv2.drawContours(img,[cnt],0,(0,0,255),-1)
    elif len(approx) == 9:
        print ("half-circle")
        cv2.drawContours(img,[cnt],0,(255,255,0),-1)
    elif len(approx) > 15:
        print ("circle")
        cv2.drawContours(img,[cnt],0,(0,255,255),-1)

cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
